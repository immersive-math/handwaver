using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

namespace IMRE.HandWaver.HigherDimensions{
  internal static class xrDisplay
  {
      public static bool isPresent()
      {
          var xrDisplaySubsystems = new List<XRDisplaySubsystem>();
          SubsystemManager.GetInstances<XRDisplaySubsystem>(xrDisplaySubsystems);
          foreach (var xrDisplay in xrDisplaySubsystems)
          {
              if (xrDisplay.running)
              {
                  return true;
              }
          }
          return false;
      }
  }
}
